import React from 'react';
import './App.css';
import Logout from './components/logout'
import {BrowserRouter} from 'react-router-dom'



function App() {
  return (
    <BrowserRouter>
      <Logout />
      </BrowserRouter>
  );
}

export default App;
