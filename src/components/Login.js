import React from "react";
import socket from "./socket";
import User from "./user";

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    let data = localStorage.getItem("myData");
    this.state = {
      username: {},
      errors: {},
      my_name: JSON.parse(data),
      socket: socket.close(),
    };
  }

  handleValidation() {
    let username = this.state.username;
    let errors = {};
    let formIsValid = true;

    if (!username["name"]) {
      formIsValid = false;
      errors["name"] = " Please Enter Username";
    }
    if (typeof username["name"] !== "undefined") {
      if (!username["name"].match(/^[a-zA-Z0-9]+$/)) {
        formIsValid = false;
        errors["name"] = "can't be empty";
      }
    }
    this.setState({ errors: errors });
    return formIsValid;
  }

  handleChange(field, e) {
    let username = this.state.username;
    username[field] = e.target.value;
    this.setState({ username });
  }

  handleSignIn(e) {
    e.preventDefault();
    if (this.handleValidation()) {
      console.log("Form submitted");
      const username = this.refs.username.value;
      this.props.onSignIn(username);
      const data = { username };
      fetch("http://localhost:7088/add/addUser", {
        method: "POST",
        mode: "cors",
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
        },
        body: JSON.stringify(data),
      })
        .then((response) => response.json())
        .then((data) => {
          // console.log('users data', data)
          localStorage.setItem("myData", JSON.stringify(data));
        })
        .catch((error) => {
          if (typeof error.json === "function") {
            error
              .json()
              .then((jsonError) => {
                console.log("Json error from API");
                console.log(jsonError);
              })
              .catch((genericError) => {
                console.log("Generic error from API");
                console.log(error.statusText);
              });
          } else {
            console.log("Fetch error");
            console.log(error);
          }
        });
    } else {
      console.log("Form has errors.");
    }
  }

  render() {
    return (
      <form id="form" onSubmit={this.handleSignIn.bind(this)}>
        <h3>Sign in</h3>
        <input
          type="text"
          ref="username"
          placeholder="enter you username"
          className="form-control"
          onChange={this.handleChange.bind(this, "name")}
          value={this.state.username["name"]}
        />
        <br />
        <span className="error text-danger"> {this.state.errors["name"]}</span>
        <br />
        <input
          id="btn"
          type="submit"
          value="Enter Chat"
          className="btn btn-lg pro border"
        />
      </form>
    );
  }
}

export default LoginForm;
