import React from "react";
import "emoji-mart/css/emoji-mart.css";
import { Picker } from "emoji-mart";
import socket from "./socket";
import Pagination from "react-js-pagination";
import ReactPaginate from "react-paginate";

const styles = {
  getEmojiButton: {
    cssFloat: "right",
    border: "none",
    margin: 0,
    cursor: "pointer",
  },
  emojiPicker: {
    position: "absolute",
    bottom: 10,
    right: 0,
    cssFloat: "right",
    marginLeft: "200px",
  },
};

class User extends React.Component {
  constructor(props) {
    super(props);
    let data = localStorage.getItem("myData");
    this.state = {
      loading: true,
      selected: false,
      msg: "",
      chat: [],
      userid: this.props.name.username,
      users: [],
      my_name: JSON.parse(data),
      selected_user: "",
      showEmojis: false,
      img: "",
      socket: socket.open(),
      file: "",
      image: "",
      video: "",
      // currentPage: "1",
      pages: "",
      offset: 0,
      data: [],
      perPage: 20,
      currentPage: 0,
      unique_user:"",
      pageCount:"",
    };
  }
  componentWillMount() {
    const { userid, selected_user, msg } = this.state;
    let { my_name } = this.state;
    const url = "http://localhost:7088/getlogin/getLogin";
    fetch(url)
      .then((response) => response.json())
      .then((users) => {
        this.setState({
          users: users.filter((i) => i.username !== userid),
          loading: false,
        });
      })
      .catch((error) => {
        if (typeof error.json === "function") {
          error
            .json()
            .then((jsonError) => {
              console.log("Json error from API");
              console.log(jsonError);
            })
            .catch((genericError) => {
              console.log("Generic error from API");
              console.log(error.statusText);
            });
        } else {
          console.log("Fetch error");
          console.log(error);
        }
      });

    // sockets

    // socket.on("chat message", ({ userid, msg }) => {
    // 	console.log('message', { userid, msg })
    // 	this.setState({
    // 		chat: [...this.state.chat, { userid, msg }]
    // 	});
    // });

    socket.emit("new-user", userid);
    socket.on("private-message", ({ selected_user, msg, userid }) => {
      // console.log("recived%%%%%%%%%%%", msg, 'to', selected_user.username,"from ",userid)
      // console.log("repeat")
      this.setState({
        chat: [...this.state.chat, { userid, msg, selected_user }],
      });
    });
    socket.on("recived-message", ({ selected_user, msg, userid }) => {
      console.log(userid, "sender");
      console.log(selected_user, "reciver");
      if (userid === this.state.selected_user)
        this.setState({
          chat: [...this.state.chat, { userid, msg, selected_user }],
        });
    });
    socket.on("addimage", ({ img, userid, selected_user, video, image }) => {
      this.setState({
        chat: [
          ...this.state.chat,
          { userid, msg, selected_user, image, video },
        ],
      });
    });

    socket.on(
      "image-recived",
      ({ img, userid, selected_user, video, image }) => {
        if (userid === this.state.selected_user)
          this.setState({
            chat: [
              ...this.state.chat,
              { img, userid, selected_user, video, image },
            ],
          });
      }
    );
  }

  addEmoji = (e) => {
    let emoji = e.native;
    this.setState({
      msg: this.state.msg + emoji,
    });
  };
  showEmojis = (e) => {
    this.setState(
      {
        showEmojis: true,
      },
      () => document.addEventListener("click", this.closeMenu)
    );
  };

  closeMenu = (e) => {
    // console.log(this.emojiPicker);
    if (this.emojiPicker !== null && !this.emojiPicker.contains(e.target)) {
      this.setState(
        {
          showEmojis: false,
        },
        () => document.removeEventListener("click", this.closeMenu)
      );
    }
  };

  user = () => {
    let user = this.state.person.map((username) => {
      return <div>{username.username} </div>;
    });
    this.setState({ users: user });
  };

  onTextChange = (e) => {
    e.preventDefault();
    this.setState({ msg: e.target.value });
  };

  UploadFile = (e) => {
    e.preventDefault();
    const file = e.target.files[0];
    var reader = new FileReader();

    if (file.size > 20971520) {
      this.setState({ file: " this file is too big" });
      this.fileInput.value = "";
    } else {
      this.setState({ file: "" });

      reader.onload = (evt) => {
        this.setState({ img: evt.target.result });
        const { img, userid, selected_user } = this.state;
        const img1 = img;
        if (img1 != null) {
          var check = img1.split("/");
          var check1 = check[0];
          //   console.log("base64 type", check1);
          if (check1 == "data:image") {
            this.setState({ image: img });
            this.setState({ video: "" });
          } else {
            // console.log("video", check1);
            this.setState({ image: "" });
            this.setState({ video: img });
          }
        }
        const { image, video } = this.state;
        socket.emit("image", { image, video, userid, selected_user });
      };
      reader.readAsDataURL(file);

      this.fileInput.value = "";
    }
  };

  onMessageSubmit = (e) => {
    e.preventDefault();
    const { userid, selected_user, msg, image, video } = this.state;
    console.log(selected_user,"sssssssssssssssssss")
    // socket.emit("chat message", { userid, msg });

    this.setState({
      msg: e.target.value,
    });
    socket.emit("private-message", { userid, selected_user, msg });
    const reciver = selected_user;
    const info = { userid, reciver, msg, image, video };
    // console.log("chat",info)

    fetch("http://localhost:7088/chatdata/chat", {
      method: "POST",
      mode: "cors",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
      body: JSON.stringify(info),
    })
      .then((response) => response.json())
      .then((info) => {
        console.log("users data", info);
      })
      .catch((error) => {
        if (typeof error.json === "function") {
          error
            .json()
            .then((jsonError) => {
              console.log("Json error from API");
              console.log(jsonError);
            })
            .catch((genericError) => {
              console.log("Generic error from API");
              console.log(error.statusText);
            });
        } else {
          console.log("Fetch error");
          console.log(error);
        }
      });
    this.setState({ msg: "" });
  };

  showMedia = (image) => {
    // console.log("media files",image)

    const url = image;
    var res = encodeURI(url);
    // console.log(image)
    // window.open(url);
    var win = window.open();
    win.document.write(
      '<iframe src="' +
        res +
        '" frameborder="0" style="border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;" allowfullscreen></iframe>'
    );
  };

  removeItem = (index,msg) => {
    console.log("click happen",index);
    let chat = this.state.chat;
    console.log(chat,"before chat");
    console.log(msg);
    chat.splice(index, 1);
    // this.setState({ chat:chat},()=>{
    //   console.log(this.state.chat,"after chat")
    // })
    // this.setState({
    //   chat: this.state.chat.filter(i => i.length != index)
    // })
    fetch(`http://localhost:7088/delete/delete/${index}`, {
      method: "DELETE",
      mode: "cors",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      }
    }).then(()=>{
      this.handleClick();
    })     
  };

  renderChat() {
    const { currentPage, chat } = this.state;
    // console.log(this.state.selected_user)
    
    return chat.map(({ userid, msg, image, video,_id}) => (
      <div>
        <div>
          <span className="font-italic" style={{ color: "#0864A4" }}>
            {userid}::{" "}
          </span>
          <span>{msg}</span>
          <br />
          {image ? (
            <img
              width="200px"
              src={image}
              onClick={(e) => this.showMedia(image)}
            />
          ) : (
            ""
          )}
          {video ? (
            <video
              controls
              width="200px"
              src={video}
              onClick={(e) => this.showMedia(video)}
            />
          ) : (
            ""
          )}
          {/* {console.log(_id)} */}
          <p key={_id}>
            <button className="btn" style={{marginLeft:"300px"}}  onClick={() => this.removeItem(_id,msg)}>
              del
            </button>
          </p>
        </div>
      </div>
    ));
  }
  handlePageClick = (e) => {
    const selectedPage = e.selected;
    const offset = selectedPage * this.state.perPage;
    // console.log(offset,"offf")

    this.setState(
      {
        currentPage: selectedPage,
        offset: offset,
      },
      () => {
        this.handleClick();
      }
    );
    // console.log(this.state.selected_user);
  };

  handleClick = () => {
    // e.preventDefault();
    let { userid, selected_user, msg, currentPage, offset } = this.state;
    console.log("++++++++++",this.state.currentPage);
    // console.log(this.state.offset);
    // console.log(user)

      console.log("selected",selected_user)

      const url2 = `http://localhost:7088/chat/userchat?user=${userid}&selected=${selected_user}&page=${currentPage}`;
    fetch(url2, {
      method: "GET",
      mode: "cors",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
    })
    // console.log(this.state.pageCount,"jhfjhdsfjhds")
      .then((response) => response.json())
      .then((result) =>
      // console.log(Math.ceil(result.length / this.state.perPage),"99999999999999999")
        this.setState({
          chat: result.data,
          pageCount:[Math.ceil(result.totalCount / this.state.perPage)]
        },()=>{

          console.log(this.state.pageCount,"jhfjhdsfjhds")
          console.log("cfau",result)
        }),
        // {
        //   const slice = result.slice(
        //     this.state.offset,
        //     this.state.offset + this.state.perPage
        //   );
        //   // console.log( this.state.offset + this.state.perPage,"offset");
        //   // console.log(this.state.perPage,"perpage");
        //   console.log(slice);

        //   const postData = slice.map((pd) => (
        //     <React.Fragment>
        //       {this.setState({
        //         chat: result,
        //       })}
        //     </React.Fragment>
        //   ));
        //   // console.log(postData)
        //   this.setState({
        //     pageCount: result.length / this.state.perPage,
        //     postData,
        //   });
        // }
      )
      .catch((error) => {
        if (typeof error.json === "function") {
          error
            .json()
            .then((jsonError) => {
              console.log("Json error from API");
              console.log(jsonError);
            })
            .catch((genericError) => {
              console.log("Generic error from API");
              console.log(error.statusText);
            });
        } else {
          console.log("Fetch error");
          console.log(error);
        }
      });
    
  

    

    
  };

  render() {
    let { users, currentPage, selected_user } = this.state;
    return (
      <div className="container-fluid">
        {this.state.loading ? (
          <div>loading..</div>
        ) : (
          <div>
            <div className="container-fluid">
              <div className="row">
                <div
                  className="col-md-3 border mt-5 overflow-auto"
                  style={{ height: "400px" }}
                >
                  <label className="text-muted">UserList</label>
                  {users.map((user, i) => {
                    return (
                      <div
                        className="user"
                        style={{ padding: "10px", cursor: "pointer" }}
                        onClick={() =>
                          //  this.handleClick(),
                        (this.setState({selected_user:user.username},()=>{
                           this.handleClick()
                        }))}
                      >
                        {user.username}
                      </div>
                    );
                  })}
                </div>
                <div className="col-md-6 m-5">
                  <div className="card">
                    {selected_user ? (
                      <>
                        <div
                          className="card-body overflow-auto"
                          id="body"
                          style={{ height: "400px" }}
                        >
                          {selected_user}
                          <hr />
                          <div className="messages">{this.renderChat()}</div>
                          <div>
                            {this.state.postData}
                            <ReactPaginate
                              previousLabel={"prev"}
                              nextLabel={"next"}
                              breakLabel={"..."}
                              breakClassName={"break-me"}
                              pageCount={this.state.pageCount}
                              onPageChange={this.handlePageClick}
                              containerClassName={"pagination"}
                              subContainerClassName={"pages pagination"}
                              activeClassName={"active"}
                            />
                          </div>
                          {/* <b>Current Page:</b>
                          <span className="ml-2">{currentPage}</span>
                          <Pagination
                            itemClass="page-item"
                            linkClass="page-link"
                            activePage={currentPage}
                            itemsCountPerPage={10}
                            totalItemsCount={400}
                            pageRangeDisplayed={5}
                            onChange={this.handlePageChange}
                          /> */}
                        </div>
                        <div className="card-footer fixed-bottom">
                          <form onSubmit={this.onMessageSubmit}>
                            <input
                              type="text"
                              placeholder="Message"
                              className="form-control"
                              onChange={(e) => this.onTextChange(e)}
                              value={this.state.msg}
                            />
                            {this.state.showEmojis ? (
                              <span
                                style={styles.emojiPicker}
                                ref={(el) => (this.emojiPicker = el)}
                              >
                                <Picker
                                  onSelect={this.addEmoji}
                                  emojiTooltip={true}
                                />
                              </span>
                            ) : (
                              <p
                                style={styles.getEmojiButton}
                                onClick={this.showEmojis}
                              >
                                {String.fromCodePoint(0x1f60a)}
                              </p>
                            )}
                            <br />
                            <input
                              type="file"
                              id="files"
                              onChangeCapture={(e) => this.UploadFile(e)}
                              ref={(ref) => (this.fileInput = ref)}
                            />
                            <label className="text-danger text-decoration-none">
                              {this.state.file}
                            </label>
                            {/* <input type="file" id="files" onChange={e => this.Uploadvid(e)} accept="video/*" /> */}
                            <input
                              id="btn"
                              type="submit"
                              className="btn btn-lg pro border"
                              value="send"
                            />
                          </form>
                        </div>
                      </>
                    ) : (
                      "SELECT USER TO CHAT"
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}
export default User;
