import React from "react";
import LoginForm from "./Login";
import User from "./user";
import { BrowserRouter, Switch, Route, Redirect, Link } from "react-router-dom";

const Welcome = ({ user, onSignOut }) => {
  return (
    <div>
      <label> Welcome To The Chat </label>
      <strong className="font-italic m-2" style={{ color: "#0864A4" }}>
        {user.username}
      </strong>
      <br />
      <a
        onClick={onSignOut}
        className="text-danger text-decoration-none"
        style={{ cursor: "pointer" }}
      >
        Leave chat
      </a>
    </div>
  );
};

class Logout extends React.Component {
  constructor(props) {
    super(props);
    let data = localStorage.getItem("myData");
    this.state = {
      user: null,
      my_name: JSON.parse(data),
      redirectToReferrer: false,
    };
    // console.log("userinfo", this.state.my_name);
  }
  signIn(username) {
    this.setState({
      user: {
        username,
      },
    });
  }
  signOut() {
    // clear out user from state
    this.setState({ user: null });
    localStorage.clear();
  }

  render() {
    const local = localStorage.getItem("myData");
    // console.log("--------", local);
    return (
      <BrowserRouter>
        <div>
          <Switch>
            {this.state.user ? (
              <div>
                <Route>
                  <Welcome
                    user={this.state.user}
                    onSignOut={this.signOut.bind(this)}
                  />
                  <Redirect to={{ pathname: "/chat" }} />
                </Route>
                <Route>
                  <User name={this.state.user} socket={this.state.socket} />
                  <Redirect to={{ pathname: "/chat" }} />
                </Route>
              </div>
            ) : local == null ? (
              <Route>
                <div>
                  <LoginForm
                    onSignIn={this.signIn.bind(this)}
                    myProp={this.state.data}
                  />
                  <Redirect to={{ pathname: "/login" }} />
                </div>
              </Route>
            ) : (
              <div>
              <Redirect to={{ pathname: "/chat" }} />
              <Welcome user={this.state.my_name}
                    onSignOut={this.signOut.bind(this)}/>
              <User name={this.state.my_name}/>
              </div>
            )}
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default Logout;
